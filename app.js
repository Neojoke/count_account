var Airtable = require('airtable');
var base = new Airtable({
    apiKey: 'keyWMDnBKbe9SHWsi'
}).base('appj9GMwhDaTT0c2P');

var total_money = 0;

var pay_count_map = {
    "任行辉": 0,
    "吴鹏": 0,
    "张文轩": 0
}
var cost_count_map = {
    "张文轩": 0,
    "吴鹏": 0,
    "任行辉": 0,
    "孙静": 0,
    "阿萍": 0
}

function startDate() {
    return process.argv.splice(2)[0];
}

function handle_record(total_count, pay_person, cost_persons) {
    total_money += total_count;
    // var tatol_count = record.fi
    pay_count_map[pay_person] = pay_count_map[pay_person] + total_count;
    var person_cost_count = total_count / cost_persons.length;
    for (const idx in cost_persons) {
        var name = cost_persons[idx];
        console.info("当前计算花费者:" + name);
        if (cost_count_map.hasOwnProperty(name)) {
            var person_cost_total_count = cost_count_map[name];
            console.info("当前计算花费者之前总花费" + person_cost_total_count);
            person_cost_total_count = person_cost_total_count + person_cost_count;
            console.info("当前计算花费者之后总花费" + person_cost_total_count);
            cost_count_map[name] = person_cost_total_count;
        }
    }
}

function check() {
    let total = 0;
    for (let item in cost_count_map) {
        total += cost_count_map[item]
    }
    console.log(total_money, '===', total);
}

function pay() {
    let 张文轩 = pay_count_map['张文轩'] - cost_count_map['张文轩'] - cost_count_map['孙静'];
    let 任行辉 = pay_count_map['任行辉'] - cost_count_map['任行辉'] - cost_count_map['阿萍'];
    let 吴鹏 = pay_count_map['吴鹏'] - cost_count_map['吴鹏'];
    console.log({
        张文轩,
        任行辉,
        吴鹏
    });

}
base(startDate())
    .select({
        // Selecting the first 3 records in Grid view:
        view: "Grid view"
    })
    .eachPage(function page(records, fetchNextPage) {
        // This function (`page`) will get called for each page of records.

        records
            .forEach(function (record) {
                console.log('record：' + typeof record);
                // console.log('record.fields["出自"]：' + record.fields["出自"] + '\n');
                // console.log('record.fields["分摊"]：' + record.fields["分摊"] + '\n');
                var total_count = record.fields["花费"];
                var pay_person = record.fields["出自"];
                var cost_persons = record.fields["分摊"];
                handle_record(total_count, pay_person, cost_persons);
            });

        // To fetch the next page of records, call `fetchNextPage`. If there are more
        // records, `page` will get called again. If there are no more records, `done`
        // will get called.
        fetchNextPage();

    }, function done(err) {
        if (err) {
            console.error(err);
            return;
        }
        console.log(pay_count_map);
        console.log(cost_count_map);
        check();
        pay();
    });